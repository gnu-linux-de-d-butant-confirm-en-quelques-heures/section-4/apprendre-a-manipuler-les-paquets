#Version 1.0

FROM ubuntu:22.04

ADD *.sh /tmp/

RUN chmod a+x /tmp/*.sh
RUN apt-get update && apt install sed git man nano vim curl gpg -y && yes | unminimize
RUN curl -fsSL "https://download.docker.com/linux/ubuntu/gpg" | gpg --dearmor --yes -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN chmod a+r /usr/share/keyrings/docker-archive-keyring.gpg

CMD ["sleep", "infinity"]