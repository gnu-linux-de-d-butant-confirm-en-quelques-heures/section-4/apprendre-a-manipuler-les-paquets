# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant de visualiser et d'installer des paquets sur Linux.

Ainsi nous verrons en détail les commandes `apt`.

<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm -p 80:80 --name man jassouline/aptitude:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

<br/>

# Exercice 1

Dans ce premier exercice, nous allons utiliser la commande `apt` pour installer des paquets.

## Installer un serveur web apache2

Un serveur web affiche le contenu d'une page, lorsqu'un utilisateur en fait la requête (par exemple lorsque vous allez sur https://www.google.fr sur votre navigateur).

Par défaut, le serveur Web écoute sur le **port 80** de la machine lorsqu'il est installé.

Vous pouvez observer en haut de la page "Play with Docker" un bouton **Open Port** dans lequel vous pouvez indiquer le port sur lequel vous voulez observer un certain contenu.

1. Grâce à la commande `apt`, installez le paquet : `apache2` sur cette machine. (n'oubliez pas de faire un `apt update` au préalable).

<details><summary> Montrer la solution </summary>
<p>
apt update
</p>
<p>
apt install apache2
</p>
</details>

2. Vous devez maintenant activer le service grâce à la commande : `service apache2 start`
   
3. Vous pouvez vérifier que l'installation s'est correctement déroulée, en allant sur **Open Port** et en indiquant le port numéro **80**. Vous devez ainsi obtenir un nouvel onglet dans lequel figure le message **It Works!**


4. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/install_verify.sh`

Si vous obtenez le message : `OK !` c'est que les actions ont été correctement effectuées.

<br/>

# Exercice 2

Dans ce deuxième exercice, nous allons utiliser la commande `apt` pour désinstaller des paquets.

## Désinstaller le serveur web apache2
Pour le moment, la page web qui s'affiche correspond à celle du service **apache2** que nous venons d'installer à l'étape précédente.

1. Grâce à la commande `apt` et à l'option `--purge`, désinstallez le paquet : `apache2` sur cette machine.

<details><summary> Montrer la solution </summary>
<p>
apt remove apache2
</p>
</details>

2. Vous pouvez vérifier que la désinstallation s'est correctement déroulée, en retournant sur l'onglet que vous avez ouvert précédemment, et en vérifiant que vous ne voyez plus la page d'accueil d'apache2.

3. Pour vérifier que vous avez correctement réussi cet exercice, tapez la commande suivante dans votre terminal :
`/tmp/remove_verify.sh`

Si vous obtenez le message : `OK !` c'est que les actions ont été correctement effectuées.

<br/>

# Exercice 3

Dans ce troisième exercice, nous allons utiliser les commandes `apt` et `grep` pour faire des recherches sur les paquets existants et les paquets installés sur notre système.

## Vérifiez les paquets installés sur le système

La commande `apt list --installed` permet d'afficher l'ensemble des paquets qui sont installés sur votre système.

*Utilisez la commande `grep` pour effectuer les recherches nécessaires pour répondre à la question ci-dessous.*

**Q1: Parmi les paquets suivants, lesquels sont installés sur le système ?**
- [ ] vim
- [ ] sed
- [ ] fdisksearch
- [ ] dpkg-search
- [ ] git

<details><summary> Montrer la solution </summary>
<p>
apt list --installed | grep vim
</p>
<p>
apt list --installed | grep sed
</p>
<p>
apt list --installed | grep git
</p>
</details>

## Trouver le nom d'un paquet à installer

On cherche maintenant à installer un paquet précis, dont le nom nous échappe...
On sait qu'il s'agit d'un paquet correspondant à un plugin **nginx** pour **certbot** en **python3**.

Grâce à la commande `apt search -n python`, je peux voir l'ensemble des paquets existants, possédant le mot **python** dans leur nom. 

Il est également possible de faire des combinaisons. Par exemple la commande `apt search -n python apache` permet de rechercher la présence à la fois des mots **python** et **apache** dans le nom du paquet.

**Q2: Quel est le nom du paquet que nous recherchons, qui correspond à un plugin nginx pour certbot en python3 ?**

<details><summary> Montrer la solution </summary>
<p>
apt search -n python3 certbot nginx
</p>
<p>
python3-certbot-nginx
</p>
</details>


# Conclusion

Tapez la commande `exit` pour sortir du conteneur, ou bien la combinaison de touches `CTRL + c`

